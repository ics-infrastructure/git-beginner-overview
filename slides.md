# Git Beginner Overview

---

## Version control

Version control is a system that **records changes** to a **file or set of files** over time so that you can recall specific versions later.

[About Version Control](https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control)

--

### Basic version control

```bash
.
├── presentation-20200503.ppt
├── presentation.ppt
└── presentation-v2.ppt
```

```bash
.
├── 20200503
│   └── presentation.ppt
└── 20200504
    └── presentation.ppt
```

Note:
This is very manual and error prone. Easy to overwrite an existing file.
Version Control Systems have been used for a long time to manage code.

--

### Centralized Version Control System

![centralized VCS](img/centralized.png) <!-- .element height="80%" width="80%" -->

Note:
CVS, Subversion
Was the standard for many years. A client only checks out one version of the project locally.

--

### Distributed Version Control Systems

![distributed VCS](img/distributed.png) <!-- .element height="40%" width="40%" -->

Note:
Git, Mercurial
Each client has the full history of the project. A local clone is a full backup.
You can interact with different servers.

---

## Git

- Created in 2005 by Linus Torvalds, the creator of Linux <!-- .element: class="fragment fade-up" -->
- Made popular by GitHub (launched in 2008): platform to host open-source projects <!-- .element: class="fragment fade-up" -->
- The most popular VCS today <!-- .element: class="fragment fade-up" -->

Note:
To replace BitKeeper a proprietary DVCS
GitHub offers Source Code Management based on Git, plus bug tracking, Pull Request workflow...
BitBucket dropped support for Mercurial

--

### Git facts

- Git is a command line tool <!-- .element: class="fragment fade-up" -->
- There are graphical clients <!-- .element: class="fragment fade-up" -->
- Can be used to version any file, not only code <!-- .element: class="fragment fade-up" -->
- Easy access thanks to services built on top of Git: GitHub, GitLab, Bitbucket <!-- .element: class="fragment fade-up" -->
- Can be used by non-developers <!-- .element: class="fragment fade-up" -->

Note:
Git is NOT GitHub or GitLab
Was created for developers
Best for text based files, but works with any format (images...)
For very large binary files, use git-lfs extension

--

### Git concepts

- repository: name of a git project (directory versioned with git) <!-- .element: class="fragment fade-up" -->
- clone: a local copy of a repository <!-- .element: class="fragment fade-up" -->
- commit: a record of a change to file(s). Identified by an id (SHA-1 hash) <!-- .element: class="fragment fade-up" -->
- tag: a reference to a commit <!-- .element: class="fragment fade-up" -->
- branch: each repo can contain multiple copies of the same project, each separate version is a branch <!-- .element: class="fragment fade-up" -->
- push/pull: commands to keep in sync a local/remote repository <!-- .element: class="fragment fade-up" -->

Note:
A git repository always have at least one branch, usually named master (community trying to change to main)
A SHA-1 is a 40 character hexadecimal string
Most commands are local

--

### Snapshots

![snapshots](img/snapshots.png)

Note:
Git doesn't store files diff but a full snapshot of all files

---

## Using Git

First time configuration:

```bash
git config --global user.name "John Doe"
git config --global user.email john.doe@ess.eu
```

--

### Creating a Git repository

```bash
$ tree -a my-devices-config/
my-devices-config
├── ODH
│   └── host3.tn.esss.lu.se.xml
├── README.md
└── cameras
    ├── another-host.tn.esss.lu.se.xml
    └── host1.tn.esss.lu.se.xml
```

--

```bash
$ cd my-devices-config
$ git init
Initialized empty Git repository in .../my-devices-config/.git/
$ git add .
$ git commit -m "Initial version"
[master (root-commit) f955f2d] Initial version
 4 files changed, 2360 insertions(+)
 create mode 100644 ODH/host3.tn.esss.lu.se.xml
 create mode 100644 README.md
 create mode 100644 cameras/another-host.tn.esss.lu.se.xml
 create mode 100644 cameras/host1.tn.esss.lu.se.xml
```

--

```bash
├── .git
│   ├── COMMIT_EDITMSG
│   ├── HEAD
│   ├── config
│   ├── description
│   ├── hooks
│   ├── index
│   ├── info
│   ├── logs
│   ├── objects
│   └── refs
├── ODH
│   └── host3.tn.esss.lu.se.xml
├── README.md
└── cameras
    ├── another-host.tn.esss.lu.se.xml
    └── host1.tn.esss.lu.se.xml
```

Note:
All git information and objects are stored under the .git directory
To share this project you need to push it somewhere, like GitLab

--

![create project](img/create-project.png)

--

![project created](img/project-created.png) <!-- .element height="60%" width="60%" -->

--

```bash
$ git remote add origin git@gitlab.esss.lu.se:benjaminbertrand/my-devices-config.git
$ git push -u origin --all
Welcome to ESS GitLab server!

Make sure to upload your ssh key to your profile to allow ssh access.
Enumerating objects: 8, done.
Counting objects: 100% (8/8), done.
Delta compression using up to 8 threads
Compressing objects: 100% (7/7), done.
Writing objects: 100% (8/8), 5.18 KiB | 2.59 MiB/s, done.
Total 8 (delta 0), reused 0 (delta 0)
To gitlab.esss.lu.se:benjaminbertrand/my-devices-config.git
 * [new branch]      master -> master
Branch 'master' set up to track remote branch 'master' from 'origin'.
```

--

![project overview](img/project-overview.png) <!-- .element height="70%" width="70%" -->

---

## GitLab

--

![project overview](img/gitlab-overview.png) <!-- .element height="90%" width="90%" -->

--

![Web IDE](img/gitlab-web-ide.png) <!-- .element height="90%" width="90%" -->

--

![Edit](img/gitlab-edit.png) <!-- .element height="90%" width="90%" -->

--

### Demo

---

## Conclusion

- Git isn't only for code. This presentation is under Git! <!-- .element: class="fragment fade-up" data-fragment-index="1" -->
  <https://gitlab.esss.lu.se/ics-infrastructure/git-beginner-overview> <!-- .element: class="fragment fade-up" data-fragment-index="1" -->
- Git is for everyone! A platform like GitLab makes it very accessible. <!-- .element: class="fragment fade-up"  data-fragment-index="2" -->
- Don't hesitate to experiment <!-- .element: class="fragment fade-up"  data-fragment-index="3" -->

Note:
There are many more things to say

---

## Questions

---

## References

- [Git website](https://git-scm.com)
- [Pro Git Book](https://git-scm.com/book/en/v2)
- [Git official documentation](https://git-scm.com/doc)
- [This presentation Git repo](https://gitlab.esss.lu.se/ics-infrastructure/git-beginner-overview)
- [GitLab on Confluence](https://confluence.esss.lu.se/display/CSI/GitLab)
- [GitLab official documentation](https://docs.gitlab.com/ee/README.html)
