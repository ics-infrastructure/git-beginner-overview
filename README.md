# Git Beginner Overview

Slides for Git Beginner Overview presentation.

Presentation available via [GitLab Pages] at: <http://ics-infrastructure.pages.esss.lu.se/git-beginner-overview/index.html>

## Getting Started

The slides are written in Mardown and rendered with [reveal.js].

To work locally:

- Edit the `slides.md` file
- Run a simple web server: `python3 -m http.server`
- Go to <http://localhost:8000> to see the slides

When pushing to the master branch, the presentation is made available via [GitLab Pages].

## Credits

- Images and content from the [Pro Git Book](https://git-scm.com/book/en/v2) are under the [Creative Commons Attribution Non Commercial Share Alike 3.0 license](https://creativecommons.org/licenses/by-nc-sa/3.0/)
- Built with [reveal.js]
- Format inspired by [GitMerge 2018 presentation](https://gitlab.com/williamchia/git-merge-2018)

[reveal.js]: https://revealjs.com
[gitlab pages]: https://docs.gitlab.com/ee/user/project/pages/
